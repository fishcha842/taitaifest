<?php
$passed=$_COOKIE["passed"];
if($passed!="TRUE")
{
	header("location:login.html");
	exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>taitaifest</title>
	<link rel="stylesheet" type="text/css" href="css/taitaifest.css">
	<link rel="stylesheet"  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src='js/taitaifesthome.js' type="text/javascript"></script>


</head>

<body style="padding-top: 95px">

	<script src="jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


	<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light" >
		<a href="index.html" class="navbar-brand">
			<img src="image/logo/logo.png" alt="taitaifestlogo" height='50px'>
		</a>

		<button type="button" class="navbar-toggler" data-toggle='collapse' data-target='#navbarContent'>
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse " id="navbarContent">
			<ul class="navbar-nav mx-auto" align='auto'>
				<li class="nav-item">
					<a href="news.html" class="nav-link">news</a>
				</li>

				<li class="nav-item">
					<a href="lineup.html" class="nav-link">lineup</a>
				</li>

				<li class="nav-item">
					<a href="timetable.html" class="nav-link">timetable</a>
				</li>
				
				<li class="nav-item" class="ticket">
					<a href="ticket.html" class="nav-link">ticket</a>
				</li>

				<li class="nav-item dropdown">
					<a href="faq.html" class="nav-link dropdown-toggle" data-toggle='dropdown'>faq</a>
					<div class="dropdown-menu">
						<a href="faq_accomondation.html" class="dropdown-item">accomondation</a>
						<a href="faq_getting_here.html" class="dropdown-item">getting here</a>
					</div>
				</li>
			</ul>

			<form class="form-inline" >
				<div>Hello, <?php echo $_COOKIE['id'] ?></div>
				<a href="logout.php" method="post" >logout</a>
				<input type="search" class="search" placeholder="search">
			</form>
		</div>
	</nav>		

	<div class="carousel slide" id="carouselarea" data-ride='carousel' style='margin-bottom:10px'>

		<ol class="carousel-indicators">
			<li data-target='#carouselarea' data-slide-to='0' class="active"></li>
			<li data-target='#carouselarea' data-slide-to='1'></li>
			<li data-target='#carouselarea' data-slide-to='2'></li>
			<li data-target='#carouselarea' data-slide-to='3'></li>
			<li data-target='#carouselarea' data-slide-to='4'></li>
		</ol>

		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class='d-block w-100' src="image/carousel/1.jpg" alt="glastonbury">
				<div class="carousel-caption">scene</div>
			</div>
			<div class="carousel-item">
				<img class='d-block w-100' src="image/carousel/2.jpg" alt="pyramid">
				<div class="carousel-caption">stage</div>
			</div>
			<div class="carousel-item">
				<img class='d-block w-100' src="image/carousel/3.jpg" alt="keane">
				<div class="carousel-caption">star</div>
			</div>
			<div class="carousel-item">
				<img class='d-block w-100' src="image/carousel/4.jpg" alt="fun">
				<div class="carousel-caption">fun</div>
			</div>
			<div class="carousel-item">
				<img class='d-block w-100' src="image/carousel/5.jpg" alt="food">
				<div class="carousel-caption">food</div>
			</div>
		</div>
	
		<a href="#carouselarea" class="carousel-control-prev" role='button' data-slide='prev'>
			<span class='carousel-control-prev-icon'></span>
		</a>
	
		<a href="#carouselarea" class="carousel-control-next" role='button' data-slide='next'>
			<span class="carousel-control-next-icon"></span>
		</a>
	</div>

<nav>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="index.html">home</a></li>
	</ol>
</nav>

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-8">
				<article class="article">
					<a href="news1.html">
						<div class="news-left">
							<img src="image/news/news1.jpg">
						</div>
						<div class="news-right">
							<h1 class="newstitle">Tones and I has been added to the lineup.</h1>
							<h3 class="date">2020/4/1</h3>
						</div>
						<div class="news-clear-both"></div>
					</a>
				</article>

				<article class="article">
					<a href="news2.html">
						<div class="news-left">
							<img src="image/news/news2.jpg">
						</div>
						<div class="news-right">
							<h1 class="newstitle">Here is the shuttle bus schedule.</h1>
							<h3 class="date">2020/3/29</h3>
						</div>
						<div class="news-clear-both"></div>
					</a>
				</article>

				<article class="article">
					<a href="news3.html">
						<div class="news-left">
							<img src="image/news/news3.jpg">
						</div>
						<div class="news-right">
							<h1 class="newstitle">Please stay hydrated.</h1>
							<h3 class="date">2020/3/17</h3>
						</div>
						<div class="news-clear-both"></div>
					</a>
				</article>
			</div>

			<div class="col-sm-6 col-md-4">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTWCDC%2F&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2646271298939503" width="350px" height="450px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

			</div>
		</div>
<hr>
		<div class="row" mx-auto>
			<div class="col-6 align-items-center">
				subscribe newletter
				<input type="email" name="useremail" placeholder="your email">
				<button type="submit">submit</button>
			</div>
			<div class="col-6 align-items-center" align="right">
				<a href="emailto:fishcha842@gmail.com"><img class="socialicon" src="image/socialicon/envelope.png"></a>
				<a href="https://www.facebook.com/"><img class="socialicon" src="image/socialicon/facebook.png"></a>
				<a href="https://www.instagram.com/?hl=zh-tw"><img class="socialicon" src="image/socialicon/instagram.png"></a>
				<a href="https://www.youtube.com/feed/trending"><img class="socialicon" src="image/socialicon/youtube.png"></a>
			</div>
		</div>
	
		<div class="row" style="margin-top: 10px">
			<div class="col">
				<p class=text-center>2020 taitaifest copyright reserved</p>
			</div>
		</div>
	</div>

</body>
</html>