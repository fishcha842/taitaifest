
export default{
	name:'goodbye',

	props:{
		named:{
			type:String,
			default:'大家'
		}
	},

	template:`
	<div>
	<div class="input-group mb-3">
	  <div class="input-group-prepend">
	    <span class="input-group-text" id="basic-addon1">@</span>
	  </div>
	  <input  v-model='named' type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
	</div>
	<p>{{named}}再見～</p>
	</div>
	`
};