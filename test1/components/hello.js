
export default{
	name:'hello',
	props:{
		named:{
			type:String,
			default:'各位',
		}
	},
	template:`
	<div>
	<div class="input-group mb-3">
	  <div class="input-group-prepend">
	    <span class="input-group-text" id="basic-addon1">@</span>
	  </div>
	  <input v-model='named' type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
	</div>
	<p>{{named}},你好！</p>
	</div>

	`
};