<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>guestbook</title>
    <script type="text/javascript">
      function check_data()
      {
        if (document.myForm.author.value.length == 0){
          alert("author is empty!");
        }else if (document.myForm.content.value.length == 0){
          alert("content is empty!");
        }else
          myForm.submit();
      }
    </script>
  </head>
  <body>
    <?php
      require_once("conn.php");

      $records_per_page = 3;

      if (isset($_GET["page"]))
        $page = $_GET["page"];
      else
        $page = 1;

      $link = create_connection();
      $sql = "SELECT * FROM guestbook ORDER BY posttime DESC";
      $result = execute_sql($link, "id12516184_taitaifest", $sql);
      $total_records = mysqli_num_rows($result);
      $total_pages = ceil($total_records / $records_per_page);
      $started_record = $records_per_page * ($page - 1);
      mysqli_data_seek($result, $started_record);

      echo "<table>";
      $j = 1;
      while ($row = mysqli_fetch_assoc($result) and $j <= $records_per_page)
      {
        echo "<tr>";
        echo "<td>作者：" . $row["author"] . "<br>";
        echo "時間：" . $row["posttime"] . "<br>";
        echo "留言：".$row["content"] . "<hr></td></tr>";
        $j++;
      }
      echo "</table>";

      echo "<p>";
      if ($page>1)
        echo "<a href='guestbook.php?page=" . ($page - 1) . "'>上一頁</a>&nbsp;";
      
      for ($i = 1; $i <= $total_pages; $i++)
      {
        if ($i == $page)
          echo "$i";
        else
          echo "<a href='guestbook.php?page=$i'>$i</a>";
      }

      if ($page < $total_pages)
        echo "&nbsp;<a href='guestbook.php?page=" . ($page + 1) . "'>下一頁</a>";
      echo "</p>";

      mysqli_free_result($result);
      mysqli_close($link);
    ?>
    <form name="myForm" action="post.php" method="post" >
      <table>
        <tr>
          <td colspan="2">please leave your message here</td>
        </tr>
        <tr>
          <td>author:</td>
          <td><input type="text" name="author"></td>
        </tr>
        <tr>
          <td>content:</td>
          <td><textarea name="content" cols="50" rows="5"></textarea></td>
        </tr>
        <tr>
          <td colspan="2">
          <input type="reset" value="rewrite">
          <input type="button" value="submit" onClick="check_data()">
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>