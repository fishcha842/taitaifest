<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>vote for headliners</title>
	<link rel="stylesheet" type="text/css" href="css/taitaifest.css">
	<link rel="stylesheet"  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script src='js/taitaifesthome.js' type="text/javascript"></script>
	<script type="text/javascript">
		function check_email(){
			newsletter.submit();
		}
	</script>
</head>
<body style="padding-top: 95px">
	<script src="jquery-3.4.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light" >
		<a href="index.html" class="navbar-brand">
			<img src="image/logo/logo.png" alt="taitaifestlogo" height='50px'>
		</a>

		<button type="button" class="navbar-toggler" data-toggle='collapse' data-target='#navbarContent'>
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse " id="navbarContent">
			<ul class="navbar-nav mx-auto" align='auto'>
				<li class="nav-item">
					<a href="news.html" class="nav-link">news</a>
				</li>

				<li class="nav-item">
					<a href="headliner.php" class="nav-link">fountain-headliners</a>
				</li>

				<li class="nav-item">
					<a href="lineup.html" class="nav-link">lineup</a>
				</li>
				
				<li class="nav-item">
					<a href="timetable.html" class="nav-link">timetable</a>
				</li>
				
				<li class="nav-item">
					<a href="ticket.php" class="nav-link">ticket</a>
				</li>

				<li class="nav-item dropdown">
					<a href="faq.html" class="nav-link dropdown-toggle" data-toggle='dropdown'>faq</a>
					<div class="dropdown-menu">
						<a href="faq_accomondation.html" class="dropdown-item">accomondation</a>
						<a href="faq_getting_here.html" class="dropdown-item">getting here</a>
					</div>
				</li>
				<li>
					<a href="guestbook.php" class="nav-link">guestbook</a>
				</li>
			</ul>

			<form class="form-inline">
				<a href="login.html" method="post" >login</a>
				<input type="search" class="search" placeholder="search">
			</form>
		</div>
	</nav>

	<nav>
	    <ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="index.html">home</a></li>
	        <li class="breadcrumb-item"><a href="headliner.php">fountain-headliner</a></li>
	    </ol>
	</nav>

	
	<div class="container-fluid text-center">
		<div class="row ">
			<div class="col">
				<p>I.Choose artists below that you would like them to be headliners:</p>
				<form name="myForm" action="vote.php" method="post">
					<div class="headliners_list">
						<?php
						require_once('conn.php');
						$link=create_connection();
						$sql="SELECT * FROM headliner";
						$result=execute_sql($link,"id12516184_taitaifest",$sql);
						while($row=mysqli_fetch_assoc($result))
						{
							echo "<input type='checkbox' name='artist' value='".$row["artist"]."'>".$row["artist"]."<br>";
						}
						mysqli_close($link);
						?>
					</div>
					<input type="button" value="vote!" onClick="javascript:myForm.submit();" >
				</form>
			</div>
		</div>
		<br>
		<div class="row" >
			<div class="col">
				<p>II.Otherwise, make a wish:</p>
				<form action="recommend.php" method="post" >
				<input type="text" name="artist" placeholder="key in an artist name" id="wish"><br>
				<input type="submit" value="add to the list" >
				</form>
			</div>
		</div>

	</div>


	<footer>
		<div class="container-fluid">
			<div class="row justify-content-around">
				<div class="col-md-4">
					<div class="footer-title">
						<p>website guideline:</p>
					</div>
					<div class="footer-content">
						<ul>
							<li>what-</li>
							<li><a href="index.html">home</a></li>
							<li><a href="news.html">news</a></li>
						</ul>

						<ul>
							<li>who-</li>
							<li><a href="headliner.php">headliners</a></li>
							<li><a href="lineup.html">lineup</a></li>
							<li><a href="timetable.html">timetable</a></li>
						</ul>

						<ul>
							<li>how-</li>
							<li><a href="ticket.php">ticket</a></li>
							<li><a href="faq.html">faq</a></li>
							<li><a href="guestbook.php">guestbook</a></li>
						</ul>

					</div>
				</div>

				<div class="col-md-4">
					<div class="footer-title">
						<p>subscribe newsletter:</p>
					</div>
					<div class="footer-content">
						<form action="newsletter.php" method="post" name='newsletter'>
						<input type="email" name="useremail" placeholder="your email">
						<input type="button" value="submit" onClick="check_email()" class="mt-2">
						</form>
					</div>
				</div>

				<div class="col-md-4" >
					<div class="footer-title">
						<p>contact us:</p>
					</div>
					
					<div class="footer-content">
						<a href="email.html"><img class="socialicon" src="image/socialicon/envelope.png"></a>
						<img class="socialicon" src="image/socialicon/facebook.png" data-toggle="modal" data-target="#exampleModalLong">
						<!-- Modal -->
						<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLongTitle">Sincerely</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body text-left" ><p>Sorry, we are thinking about not establishing a fb account, since it is full of old people only.</p>
						      </div>
						    </div>
						  </div>
						</div>
						<a href="https://www.instagram.com/?hl=zh-tw"><img class="socialicon" src="image/socialicon/instagram.png"></a>
						<a href="https://www.youtube.com/feed/trending"><img class="socialicon" src="image/socialicon/youtube.png"></a>
					</div>
				</div>
			</div>
		<hr>
			<div class="row" >
				<div class="col copyright">
					<p>2020 taitaifest copyright reserved</p>
				</div>
			</div>

		</div>
	</footer>

</body>
</html>

<!-- onclick="javascript:myForm2.submit(); -->